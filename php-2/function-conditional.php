<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Function</title>
</head>
<body>
<h1>Berlatih Function PHP</h1>
<?php

function greetings($name) {
    echo "Halo $name, Selamat Datang di Garuda Cyber Institute!<br>";
}

echo "<h3>Soal No 1 Greetings</h3>";

// Panggil fungsi greetings dengan nama yang berbeda-beda
greetings("teguh");
greetings("rohan");
greetings("nama peserta");

function reverseString($str) {
    $length = strlen($str);
    $reversedStr = '';
    $i = $length - 1;

    while ($i >= 0) {
        $reversedStr .= $str[$i];
        $i--;
    }

    echo $reversedStr . "<br>";
}

echo "<h3>Soal No 2 Reverse String</h3>";

reverseString("abdul");
reverseString("nama peserta");
reverseString("Garuda Cyber Institute");
reverseString("We Are GC-Ins Developer");

function reverseString2($str) {
    $length = strlen($str);
    $reversedStr = '';

    // Loop dari karakter terakhir ke pertama
    for ($i = $length - 1; $i >= 0; $i--) {
        $reversedStr .= $str[$i];
    }

    return $reversedStr;
}

function palindrome($str) {
    $reverse = reverseString2($str);
    if ($str == $reverse) {
        return "true";
    } else {
        return "false";
    }
}


echo "<h3>Soal No 3 Palindrome </h3>";

// Contoh pemanggilan fungsi palindrome dengan beberapa string
echo "palindrome(\"civic\") ;  " . palindrome("civic") . "<br>";
echo "palindrome(\"nababan\") ;  " . palindrome("nababan") . "<br>";
echo "palindrome(\"jambaban\");  " . palindrome("jambaban") . "<br>";
echo "palindrome(\"racecar\");  " . palindrome("racecar") . "<br>";

echo "<h3>Soal No 4 Tentukan Nilai </h3>";

/* Soal 4
buatlah sebuah function bernama tentukan_nilai . Di dalam function tentukan_nilai yang menerima parameter
berupa integer. dengan ketentuan jika paramater integer lebih besar dari sama dengan 85 dan lebih kecil sama dengan 100 maka akan mereturn String “Sangat Baik” Selain itu jika parameter integer lebih besar sama dengan 70 dan lebih kecil dari 85 maka akan mereturn string “Baik” selain itu jika parameter number lebih besar sama dengan 60 dan lebih kecil dari 70 maka akan mereturn string “Cukup” selain itu maka akan mereturn string “Kurang”
*/
// Code function di sini
function tentukan_nilai($nilai) {
    if ($nilai >= 85 && $nilai <= 100) {
        return "Sangat Baik";
    } elseif ($nilai >= 70 && $nilai < 85) {
        return "Baik";
    } elseif ($nilai >= 60 && $nilai < 70) {
        return "Cukup";
    } else {
        return "Kurang";
    }
}


// Contoh pemanggilan fungsi tentukan_nilai dengan beberapa nilai
echo "echo tentukan_nilai(98); " . tentukan_nilai(98) . "<br>";
echo "echo tentukan_nilai(76); " . tentukan_nilai(76) . "<br>";
echo "echo tentukan_nilai(67); " . tentukan_nilai(67) . "<br>";
echo "echo tentukan_nilai(43); " . tentukan_nilai(43) . "<br>";

?>

</body>
</html>
